<?php
// 图像的像素处理

namespace JyOcr;

class ImagePixel extends ImageInfo
{
  /**
   * 获取图片主要颜色
   *
   * @return array
   */
  public function getAllColorList()
  {
    $list  = [];
    $count = 0;
    for ($x = 0; $x < imagesx($this->im); $x++) {
      for ($y = 0; $y < imagesy($this->im); $y++) {
        $count++;
        $rgb = imagecolorat($this->im, $x, $y);
        if ($rgb == 0) {
          $list[$x][$y] = [
            'r'   => 0,
            'g'   => 0,
            'b'   => 0,
            'rgb' => 0,
          ];
        } else {
          $list[$x][$y] = [
            'r'   => ($rgb >> 16) & 0xFF,
            'g'   => ($rgb >> 8) & 0xFF,
            'b'   => $rgb & 0xFF,
            'rgb' => $rgb,
          ];
        }
      }
    }
    return $list;
  }
  
  /**
   * 获取图片主要颜色(一维数组)
   *
   * @return array
   */
  public function getAllColorList1()
  {
    $list = [];
    for ($x = 0; $x < imagesx($this->im); $x++) {
      for ($y = 0; $y < imagesy($this->im); $y++) {
        $list[$x . '_' . $y] = imagecolorat($this->im, $x, $y);
      }
    }
    return $list;
  }
  
  /**
   * 取一个10进制颜色值的RGB
   *
   * @param int $color 10进制的颜色值
   * @return int[]
   */
  public function getRGB($color)
  {
    return [
      $color & 0xFF,          // b
      ($color >> 8) & 0xFF,   // g
      ($color >> 16) & 0xFF,  // r
    ];
  }
  
  /**
   * 取两个颜色的相似度
   *
   * @param int $color1 10进制的颜色值
   * @param int $color2 10进制的颜色值
   * @return float
   */
  public function getSimilarity($color1, $color2)
  {
    [$R_1, $G_1, $B_1] = $this->getRGB($color1);
    [$R_2, $G_2, $B_2] = $this->getRGB($color2);
    $rmean = ($R_1 + $R_2) / 2;
    $R     = $R_1 - $R_2;
    $G     = $G_1 - $G_2;
    $B     = $B_1 - $B_2;
    return sqrt((2 + $rmean / 256) * pow($R, 2) + 4 * pow($G, 2) + (2 + (255 - $rmean) / 256) * pow($B, 2));
  }
}
