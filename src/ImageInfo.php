<?php
// 获取图片的相关信息

namespace JyOcr;

class ImageInfo
{
  protected $im;
  protected $info;
  protected $width;
  protected $height;
  protected $type;
  
  public function __construct($imageFile)
  {
    $this->im = $this->createFromImage($imageFile);
  }
  
  public function __destruct()
  {
    imagedestroy($this->im);
  }
  
  public function changeImageFile($imageFile)
  {
    $this->im = $this->createFromImage($imageFile);
  }
  
  public function setIm($im)
  {
    $this->im = $im;
  }
  
  public function getIm()
  {
    return $this->im;
  }
  
  public function getHeight()
  {
    return $this->height;
  }
  
  public function getWidth()
  {
    return $this->width;
  }
  
  public function getType()
  {
    return $this->type;
  }
  
  public function getInfo()
  {
    return $this->info;
  }
  
  protected function createFromImage($imageFile)
  {
    if (file_exists($imageFile) && !is_dir($imageFile)) {
      $info         = getimagesize($imageFile);
      $this->width  = $info[0];
      $this->height = $info[1];
      $this->type   = $info[2];
      $this->info   = [
        'type'   => $this->type,
        'width'  => $this->width,
        'height' => $this->height,
      ];
      if ($this->type > 3) {
        throw new Exception('只支持jpg,png,gif这三种格式的图像文件');
      }
      
      switch ($this->type) {
        // gif
        case IMAGETYPE_GIF:
          $image = imagecreatefromgif($imageFile);
          break;
        // jpg
        case IMAGETYPE_JPEG:
          $image = imagecreatefromjpeg($imageFile);
          break;
        // png
        case IMAGETYPE_PNG:
          $image = imagecreatefrompng($imageFile);
          break;
      }
    } else {
      throw new Exception('不是一个有效的图像文件');
    }
    return $image;
  }
}
